# Requirements

## Yarn

[See Installation instruction](https://yarnpkg.com/en/docs/install)

```
brew install yarn
```

## Other tools

* [Visual Studio Code](https://code.visualstudio.com/)
  * [ESLint](https://marketplace.visualstudio.com/items?itemName=dbaeumer.vscode-eslint)
  * [Prettier](https://marketplace.visualstudio.com/items?itemName=esbenp.prettier-vscode)
* [Postman](https://www.getpostman.com/downloads/)
* [Altair GraphQL Client](https://altair.sirmuel.design/)
* [Docker](https://www.docker.com)

# Examples

## Express

```
yarn add \
  express \
  body-parser \
  multer \
  jest \
  @types/jest \
  @types/express \
  @types/body-parser \
  @types/multer
```

## TypeScript & ESLint

```
yarn add \
  @typescript-eslint/eslint-plugin \
  @typescript-eslint/parser \
  eslint \
  eslint-config-prettier \
  eslint-plugin-import \
  eslint-plugin-prettier \
  prettier \
  ts-node \
  typescript \
  env-cmd \
  @types/node
```

NPM-Script:

```
...
"lint": "yarn run eslint 'src/**/*.{ts,tsx}'",
...
```

## Jest

`package.json`

```
"jest": {
  "testEnvironment": "node",
  "testMatch": [
    "<rootDir>/src/**/__tests__/**/*.test.ts"
  ],
  "transform": {
    "^.+\\.(ts|tsx)$": "<rootDir>/__tests__/preprocessor.js"
  }
},
```

`__tests__/preprocessor.js`:

```
const tsc = require('typescript');
const tsConfig = require('../tsconfig.json');

module.exports = {
  process(src, path) {
    if (path.endsWith('.ts') || path.endsWith('.tsx')) {
      return tsc.transpile(src, tsConfig.compilerOptions, path, []);
    }
    return src;
  },
};
```

## [TypeDI](https://github.com/typestack/typedi)

```
yarn add \
  reflect-metadata \
  typedi
```

## [Routing-Controllers](https://github.com/typestack/routing-controllers)

```
yarn add \
  reflect-metadata \
  class-transformer \
  class-validator \
  routing-controllers
```

## MySQL

```
docker-compose up
```

## [TypeORM](https://typeorm.io/)

```
yarn add \
  reflect-metadata \
  class-validator \
  typeorm \
  mysql \
  typeorm-typedi-extensions

yarn run typeorm init --db mysql
```

## [GraphQL](https://graphql.org/learn/) & [TypeGraphQL](https://typegraphql.ml/)

```
yarn add \
  reflect-metadata \
  graphql \
  type-graphql \
  @types/graphql
```

## [Nest](https://nestjs.com/)

```
npm i -g @nestjs/cli
nest new [name] -g -p yarn
cd [name]
yarn add class-validator class-transformer
```

### Microservice

```
yarn add @nestjs/microservices
```

### GraphQL

```
yarn add @nestjs/graphql graphql-tools graphql type-graphql apollo-server-express
```

## [Serverless](https://serverless.com/)

See also: [Awesome Serverless](https://github.com/pmuens/awesome-serverless)

```
yarn add -g serverless
mkdir lambda-test && cd lambda-test
serverless create -t aws-nodejs-typescript
yarn install
```

### Useful Serverless plugins

- [Serverless offline](https://github.com/dherault/serverless-offline)
- [AWS Nested Stacks](https://github.com/concon121/serverless-plugin-nested-stacks)
- [Canary Deployments](https://github.com/davidgf/serverless-plugin-canary-deployments)
- [Localstack](https://github.com/localstack/localstack)

# Frameworks

- [Express.js](https://expressjs.com/de/)
- [Hapi](https://hapijs.com/)
- [Nest.js](https://nestjs.com/)

# Useful Links

## Blogs

- [Nodejs.dev](https://nodejs.dev/)
- [RisingStack Blog](https://blog.risingstack.com/)
- [Twilio Blog](https://www.twilio.com/blog/)
- [AirBnB Engineering](https://medium.com/airbnb-engineering)
- [Netflix Tech Blog](https://medium.com/netflix-techblog)

## Packages

- [Delightful Node.js packages and resources](https://github.com/sindresorhus/awesome-nodejs)
- [Loggin library Winston](https://www.npmjs.com/package/winston)
- [bignumber.js](https://www.npmjs.com/package/bignumber.js)
- [env-cmd](https://www.npmjs.com/package/env-cmd)
- [jsonwebtoken](https://www.npmjs.com/package/jsonwebtoken)
- [lodash](https://www.npmjs.com/package/lodash)
- [luxon](https://www.npmjs.com/package/luxon)
- [date-fns](https://www.npmjs.com/package/date-fns)
- [peg-js](https://www.npmjs.com/package/pegjs)
- [validator.js](https://www.npmjs.com/package/validator)
- [axios](https://www.npmjs.com/package/axios)
- [commander](https://www.npmjs.com/package/commander)
